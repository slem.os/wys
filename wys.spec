Name:           wys
Version:        0.1.7
Release:        7%{?dist}
Summary:        WYS audio manage for Pinephone

License:        GPLv3+
URL:            https://source.puri.sm/Librem5/wys
%undefine _disable_source_fetch
Source0:        https://source.puri.sm/Librem5/wys/-/archive/v%{version}/%{name}-v%{version}.tar.gz
Source1:        wys.service

Patch0:         0001-Simplify-daemon-to-only-switch-card-profiles.patch 
BuildRequires:  gcc
BuildRequires:  meson
BuildRequires:  pam-devel
BuildRequires:  pkgconfig(alsa)
BuildRequires:  pkgconfig(glib-2.0) >= 2.50.0
BuildRequires:  pkgconfig(gobject-2.0) >= 2.50.0
BuildRequires:  pkgconfig(libpulse) >= 2.0
BuildRequires:  pkgconfig(libpulse-mainloop-glib)
BuildRequires:  pkgconfig(polkit-agent-1) >= 0.105
BuildRequires:  pkgconfig(ModemManager)
BuildRequires:  git
BuildRequires:  systemd

Requires:       gnome-session
Requires:       lato-fonts

%description
WYS package for audio and calls profiles

%prep
%setup -q -n %{name}-v%{version}
%patch -p1

%build
%meson
%meson_build


%install
%meson_install
mkdir -p $RPM_BUILD_ROOT/usr/lib/systemd/user/
cp %{SOURCE1} $RPM_BUILD_ROOT/usr/lib/systemd/user/

%post
%systemd_post wys.service
systemctl --user enable wys.service


%preun
%systemd_preun wys.service


%postun
%systemd_postun_with_restart wys.service


%files
%{_bindir}/wys
%dir /usr/lib/systemd/user
/usr/lib/systemd/user/wys.service


%changelog
* Wed Jun 25 2020 Adrian Campos <adriancampos@teachelp.com> - 0.3.1-7
- First build
